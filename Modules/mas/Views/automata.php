<?php 
  global $path;
  /*$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => "http://localhost:8008/"
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl);
		$resp=strtr ($resp, array ("'" => '"'));
	
   */



?>


<link rel="stylesheet" type="text/css" href="<?php echo $path;?>Modules/mas/script/joint.css" />
<script type="text/javascript" src="<?php echo $path;?>Modules/mas/script/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $path;?>Modules/mas/script/lodash.min.js"></script>
<script type="text/javascript"  src="<?php echo $path;?>Modules/mas/script/backbone-min.js"></script>
<script type="text/javascript"  src="<?php echo $path;?>Modules/mas/script/joint.js"></script>

<script type="text/javascript" src="<?php echo $path;?>Modules/mas/Views/mas.js"></script>



<html>
<body>
	<table >
		<tr><td>
<div id="paper" class="paper"></div></td>
<td><table border=1>

<tr><td>DeviceID</td><td id="device"></td></tr>
<tr><td>OperatingType</td><td id="operatingtype"></td></tr>
<tr><td>Status</td><td id="status"></td></tr>
<tr><td>Power</td><td id="power"></td></tr>
<tr><td>Modes</td><td id="modes"></td></tr>
<tr><td>Current Mode</td><td id="modeName"></td></tr>
<tr><td>Noise</td><td id="noise"></td></tr>
<tr><td>Seconds since last power</td><td id="power0"></td></tr>
<tr><td>Load ID</td><td id="loadid"></td></tr>
<tr><td>Profile</td><td id="profile">  </td></tr>
<tr><td>Warnings</td><td id="warning"></td></tr>
</table></td>
</tr>
</table>
</body></html>


<script>
var path = "<?php echo $path; ?>";
var name = "<?php echo $_GET['profile'];?>";




var graph = new joint.dia.Graph();

var paper = new joint.dia.Paper({
    el: $('#paper'),
    width: 800,
    height: 600,
    gridSize: 1,
    model: graph
});

function state(x, y, label,color) {
    
    var cell = new joint.shapes.fsa.State({
        position: { x: x, y: y },
        size: { width: 60, height: 60 },
        attrs: {
            text : { text: label, fill: '#000010', 'font-weight': 'normal' },
            'circle': {
                fill: 'white',
                stroke: color,
                'stroke-width': 2
            }
        }
    });
    graph.addCell(cell);
    return cell;
}

function link(source, target, label, vertices) {
    
    var cell = new joint.shapes.fsa.Arrow({
        source: { id: source.id },
        target: { id: target.id },
        labels: [{ position: 0.5, attrs: { text: { text: label || '', 'font-weight': 'bold' } } }],
        vertices: vertices || [],
        
    });
    graph.addCell(cell);
    return cell;
}

var start = new joint.shapes.fsa.StartState({ position: { x: 50, y: 350 } });
graph.addCell(start);

var off  = state(180, 250, 'off');
var on = state(260, 100, 'on');
var wait  = state(450, 250, 'wait');
//var line  = state(340, 240, 'line');
//var block = state(560, 140, 'block');

var lst_off=link(start, off,  'power<noise,-');
link(off,  off,  'power<noise,-', [{x: 180,y: 400}, {x: 305, y: 400}]);

var off_on=link(off,  on, 'power>noise,!default',[{x: 100,y: 250}, {x: 120, y: 150}]);
link(on, on,  'power>noise,-',[{x: 190,y: 70}, {x: 300, y: 50}]);
link(on, off,  'power<noise,0_power>3', [{x: 350, y: 200}]);

link(off, wait,  'power>noise,default', [{x: 350, y: 330}]);

link(wait, wait,  'power<noise,-', [{x: 580, y: 340},{x: 580, y: 210}]);
link(wait, on,  'power<noise,time>AST', [{x: 500,y: 150}, {x: 450, y: 100}]);
var current_status=0;	

update();

updater = setInterval(update, 5000);

  

function update()
  {
	  console.log("updating");
      var profile= mas.profileinfo(name);
      $('#power').text(profile['power']);
	  $('#status').text(profile['status']);
      $('#device').text(profile['deviceid']);
      $('#operatingtype').text(profile['operatingType']);
      $('#modes').text(profile['modes']);
      $('#modeName').text(profile['modeName']);
      $('#noise').text(profile['noise']);
      $('#power0').text(parseInt(profile['time'])-parseInt(profile['end']));
      $('#loadid').text(profile['loadid']);
      
      var ot = profile['operatingType'];
      
      if(profile['operatingType'] == "continuously_run")
      {
          $('#profile').text("off_duration: "+profile['off_duration']+", on_duration: "+profile['on_duration']);
          }
       
       
      switch(current_status)
      {
          case 0:
                    on.attr("circle/stroke",'black');
                    break;
          
          }
    
      var next_status=parseInt(profile['status']);
      switch(next_status)
      {
          case 0:
                    off_on.attr("stroke",'black');
                    off.attr("circle/stroke",'red');
                    break;
          case 1:   if(current_status==0) off_on.attr("stroke",'red');
                    on.attr("circle/stroke",'red');
                    break;
          case 2:   console.log("2222222");
                    on.attr("circle/stroke",'black');
                    wait.attr("circle/stroke",'red');
                    break;
      }
      current_status=next_status;
      
      
 
}
//link(on, line,  '/');
//link(line,  off,  'new\n line');
//link(on, block, '*');
//link(block, star,  '*');
//link(star,  block, 'other', [{x: 650, y: 290}]);
//link(star,  off,  '/',     [{x: 490, y: 310}]);
//link(line,  line,  'other', [{x: 115,y: 100}, {x: 250, y: 50}]);
//link(block, block, 'other', [{x: 485,y: 140}, {x: 620, y: 90}]);


</script>
