#!/bin/bash
if [[ $EUID -ne 0 ]]; then
       echo "This script must be run as root" 
          exit 1
fi

#installs dependencies
apt-get update
apt-get install python -y 
apt-get install python-pip -y 
apt-get install python-MySQLdb -y


#installs crontab
(crontab -l -u pi; echo "0 * * * * /var/www/emoncms/Modules/fault-detection/cron") | uniq - | crontab -u pi –