#!/usr/bin/python
import urllib2
#import requests
import re
import six.moves.configparser
from datetime import datetime
from time import time
import socket
import MySQLdb
import sys

sys.stderr = open('error.log', 'a')

config = six.moves.configparser.RawConfigParser()
try:
    config.read('setup.cfg')
except:
    sys.exit("No setup file found")

#The EmonCMS userID, it defaults to one
uid = str(config.get('EmonCMS', 'userID'))
#too_high and too_low are percent values that define thresholds before the users should be altered
too_high = float(config.get('Values', 'too_high'))
too_low = float(config.get('Values', 'too_low'))

dp = str(config.get('Values', 'datapoints'))
#the whitelist is the list of feeds that shall be monitored
wl = str(config.get('Whitelist','whitelist')).split()
whitelist = []
for i in range(0, len(wl)):
    whitelist.append(wl[i].split(','))
if not whitelist:
    #Quits the program with an error if no feeds are in the whitelist
    sys.exit(str(datetime.today())+"\nNo whitelisted feeds\n")
addr = '127.0.0.1'
#The following line finds the EmonCMS public IP address
#addr = [l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1],[[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET,socket.SOCK_DGRAM)]][0][1]]) if l][0][0]

#gets the EmonCMS write apikey
def getapikey():
    with open("/var/www/emoncms/settings.php", 'r') as settings:
        for line in settings:
            if "$server" in line:
                server = line[17:-3]
            elif "$database" in line:
                database = line[17:-3]
            elif "$username" in line:
                username = line[17:-3]
            elif "$password" in line:
                password = line[17:-3]
    try:
        db = MySQLdb.connect(host=server, user=username, passwd=password, db=database)
        cursor = db.cursor()
        cursor.execute("SELECT apikey_read FROM users;")
        apikey = str(cursor.fetchone())[2:-3]
        cursor.close()
        db.close()
    except:
        send_mail("APIkey Error", "Could not get the APIkey, make sure")
        sys.exit(str(datetime.today())+"\nCouldn't get APIkey\n")
    return apikey
apikey = getapikey()

#recreates the json array as a python array
def json_to_array(url, split_char):
    req = urllib2.urlopen(url)

    req_text= str(req.read())

    req_text = req_text.split(split_char)
    req_text[0]=req_text[0].lstrip('{[')
    req_text[-1]=req_text[-1].rstrip(']}')
    new_req_text=[]
    for r in req_text:
        text_split = r.split(',')
        new_req_text.append(text_split)
    return new_req_text

#TODO: this is implements a way to send mail via pythons mail api, should be changed to send from the raspberry pi
def send_mail(SUBJECT, TEXT):
    import smtplib
    user = config.get('Email', 'from_addr')
    pwd = config.get('Email', 'pwd')
    smtp_server = config.get('Email', 'smtp_server')
    port = config.get('Email', 'port')
    FROM = config.get('Email', 'from')
    TO = [config.get('Email', 'to_addr')] #must be a list
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s""" % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP(smtp_server, port)
        server.ehlo()
        #unncommet to use TLS
        #server.starttls()
        server.login(user, pwd)
        server.sendmail(FROM, TO, message)
        server.close()
    except:
        sys.stderr.write(str(datetime.today())+"\nMail was not sent\n")

ids = []
old_ids = []
removed_ids= []
new_ids = []
new = False
removed = False
for i in range(0,len(whitelist)):
    ids.append(whitelist[i][0])
#gets all the feeds
try:
    feeds = json_to_array('http://'+addr+'/emoncms/feed/list.json?userid='+uid+'&apikey='+apikey,'},{')
    for f in feeds:
        for i in ids:
            if int((re.sub(r'[^\d.]+','',f[0]))) != int(i):
                pass
            else:
                if (f[8] == '"time":false') or (f[9] == '"value":""') or (f[9]== '"value":"0"'):
                    ids.remove(i)
                elif int(re.sub(r'[^\d.]+','',f[8])) - int(round(time())*1000) > (3600000):
                    ids.remove(i)
except:
    send_mail("Feed error", "Could not get any feeds")
    sys.exit(str(datetime.today())+"\nCouldn't get feeds/n")
#removes inactive feeds
tmp = []
for i in ids:
    for w in whitelist:
        if int(w[0]) == int(i):
            tmp.append(w)
whitelist = tmp

#compares list of active feeds with the list of previous active feeds

with open("Active_feeds.txt", "w") as text_file:
	for i in ids:
		text_file.write(str(i))
		text_file.write('\n')

with open("Active_feeds.old","r") as old_file:
    old_lines = old_file.readlines()
    for o in old_lines:
        old_ids.append(o.rstrip())

for i in ids:
    if i not in old_ids:
        new_ids.append(i)
        new = True
for o in old_ids:
    if o not in ids:
        removed_ids.append(o)
        removed = True

#writes a log of changes made to the active feeds to disk and sends mail if any are removed
with open("Active_feeds.log", "a") as log_file:
    log_file.write("\nTimestamp: "+str(datetime.today()) +'\n')
    if (not removed and not new):
        log_file.write("Nothing changed\n")
    else:
         if removed:
            for r in removed_ids:
                log_file.write("Removed feed: "+str(r)+'\n')

            mail_subject = str(datetime.today())+ ' '+config.get('Email', 'subject')
            mail_body = "Fault detected, feed(s) "+str(removed_ids)+" removed from Active feeds"
            send_mail(mail_subject, mail_body)
         if new:
            for n in new_ids:
                log_file.write("New feed: "+str(n)+'\n')

#checks for irregularities in the feeds and sends mail if any are encountred
for i in whitelist:
    end = str(int(round(time())*1000))
    start = str(int(end)-int(i[1])*3600*1000)

    try:
        feed = json_to_array('http://'+addr+'/emoncms/feed/average.json?id='+i[0]+'&start='+start+'&end='+end+'&dp='+dp+'&apikey='+apikey,'],[')
        with open('feeds/feed'+i[0]+'.log', "w") as feed_file:
            for f in feed:
                f = str(f)
                feed_file.write(f+'\n')
    except:
        body = "Couldn't connect to EmonCMS, please contact an administrator to check on it"
        subject = "EmonCMS logging error"
        send_mail(subject, body)

for i in ids:
    try:
        with open('feeds/feed'+i+'.log', 'r') as file:
            lines = file.readlines()
            for l in lines:
                if float(l[19:-3]) > too_high:
                    send_mail("Feed value too high", "Feed "+str(i)+" is reporting a value above the set threshold and should be investigated")
                elif float(l[19:-3]) < too_low:
                    send_mail("Feed value too low", "Feed "+str(i)+" is reporting a value bellow the set threshold and should be investigated")
                else:
                    pass
    except:
        pass
