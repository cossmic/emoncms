#!/usr/bin/python
import urllib2
#import requests
import re
import six.moves.configparser
from datetime import datetime
from time import time
import socket
import MySQLdb
import sys

sys.stderr = open('error.log', 'a')

config = six.moves.configparser.RawConfigParser()
try:
    config.read('setup.cfg')
except:
    sys.exit("No setup file found")

#The EmonCMS userID, it defaults to one
uid = str(config.get('EmonCMS', 'userID'))
#too_high and too_low are percent values that define thresholds before the users should be altered
dp = str(config.get('Values', 'datapoints'))
#the whitelist is the list of feeds that shall be monitored
wl = str(config.get('Whitelist','whitelist')).split()
whitelist = []
for i in range(0, len(wl)):
    whitelist.append(wl[i].split(','))
if not whitelist:
    #Quits the program with an error if no feeds are in the whitelist
    sys.exit(str(datetime.today())+"\nNo whitelisted feeds\n")
#addr = '127.0.0.1'
addr = '10.0.0.17'

#The following line finds the EmonCMS public IP address
#addr = [l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1],[[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET,socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
print whitelist
#gets the EmonCMS read apikey
def getapikey():
    with open("/var/www/emoncms/settings.php", 'r') as settings:
        for line in settings:
            if "$server" in line:
                server = line[17:-3]
            elif "$database" in line:
                database = line[17:-3]
            elif "$username" in line:
                username = line[17:-3]
            elif "$password" in line:
                password = line[17:-3]
    try:
        db = MySQLdb.connect(host=server, user=username, passwd=password, db=database)
        cursor = db.cursor()
        cursor.execute("SELECT apikey_read FROM users;")
        apikey = str(cursor.fetchone())[2:-3]
        cursor.close()
        db.close()
    except:
        send_mail("APIkey Error", "Could not get the APIkey, make sure")
        sys.exit(str(datetime.today())+"\nCouldn't get APIkey\n")
    return apikey
#apikey = getapikey()
apikey = 'eaa887af16d202e4458dcd60c0b229ac'

#recreates the json array as a python array
def json_to_array(url, split_char):
    req = urllib2.urlopen(url)

    req_text= str(req.read())

    req_text = req_text.split(split_char)
    req_text[0]=req_text[0].lstrip('{[')
    req_text[-1]=req_text[-1].rstrip(']}')
    new_req_text=[]
    for r in req_text:
        text_split = r.split(',')
        new_req_text.append(text_split)
    return new_req_text

#TODO: this is implements a way to send mail via pythons mail api, should be changed to send from the raspberry pi
def send_mail(SUBJECT, TEXT):
    import smtplib
    user = config.get('Email', 'from_addr')
    pwd = config.get('Email', 'pwd')
    smtp_server = config.get('Email', 'smtp_server')
    port = config.get('Email', 'port')
    FROM = user
    TO = [config.get('Email', 'to_addr')] #must be a list
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s""" % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP(smtp_server, port)
        server.ehlo()
        #unncommet to use TLS
        #server.starttls()
        server.login(user, pwd)
        server.sendmail(FROM, TO, message)
        server.close()
    except:
        sys.stderr.write(str(datetime.today())+"\nMail was not sent\n")

def checkFeeds():
	end = int(round(time())*1000)
	print "end: "+str(end)
	for w in whitelist:
		low = True
		high = True
		print "feed: "+str(w[0])
		start = end - 60000*int(w[1])
		print "start: "+str(start)
		feed = json_to_array('http://'+addr+'/emoncms/feed/average.json?id='+w[0]+'&start='+str(start)+'&end='+str(end)+'&dp='+dp+'&apikey='+apikey, '],[')
		for f in feed:
			if str(f[0]) == '"success":false':
				send_mail("Bad feed", "Feed "+str(w[0])+" does not exist. Consider removing it from the whitlist untill it is fixed.")
				low = False
				high = False
				break 
			elif float(f[1]) > float(w[2]):
				low = False
			elif float(f[1]) < float(w[3]):
				high = False
		if low == True:
			send_mail("Suspecious feed", "Feed "+str(w[0])+" is reporting low values and should be invesetigated")
		elif high == True:
			send_mail("Suspecious feed", "Feed "+str(w[0])+" is reporting high values and should be invesetigated")

def loging():
	pass

checkFeeds()

#send_mail("Hallo", "test")