<?php
/*
 All Emoncms code is released under the GNU Affero General Public License.
 See COPYRIGHT.txt and LICENSE.txt.

 ---------------------------------------------------------------------
 Emoncms - open source energy visualisation
 Part of the OpenEnergyMonitor project:
 http://openenergymonitor.org
 */

// no direct access
defined('EMONCMS_EXEC') or die('Restricted access');


class Devices
{
	private $mysqli;
	private $input;
	private $redis;
	private $feed;
	private $process;
	private $log;
	
	public function __construct($mysqli, $redis, $input, $feed, $process)
	{
		$this->mysqli = $mysqli;
		$this->input = $input;
		$this->redis = $redis;
		$this->feed = $feed;
		$this->process = $process;
		$this->log = new EmonLogger(__FILE__);
	}
	
	public function exists($deviceid)
	{
	  $deviceid = (int) $deviceid;
	  $result = $this->mysqli->query("SELECT deviceid FROM devices WHERE `deviceid` = '$deviceid'");
	  if ($result->num_rows > 0) return true; else return false;
	}
	
	public function belongs_to_user($userid, $deviceid)
	{
		$userid = (int) $userid;
		$deviceid = (int) $deviceid;

		$result = $this->mysqli->query("SELECT deviceid FROM devices WHERE userid = '$userid' AND deviceid = '$deviceid'");
		if ($result->fetch_array()) return true; else return false;
	}
	
	public function get_template($userid, $templateid)
	{
		if (!is_numeric($templateid)) return array('success'=>false, 'message'=>'Invalid template ID');
		
		$qresult = $this->mysqli->query("SELECT * FROM templates WHERE userid = $userid and templateid=".$templateid);
		
		if($row = $qresult->fetch_object()) {

			$row->id = $row->templateid;
			$row->requiredNodeTypes = explode(',', $row->requiredNodeTypes);
			$row->modes = $this->get_template_modes($userid, $templateid);
			return $row;
		}
		return array('success'=>false, 'message'=>'Template does not exist');
	}
	
	public function list_templates($userid)
	{
		$qresult = $this->mysqli->query("SELECT * FROM templates WHERE userid = $userid");

		$names = array();
		$templates = array();
		while($row=$qresult->fetch_object()) {
			$row->id = $row->templateid;
			$row->requiredNodeTypes = explode(',', $row->requiredNodeTypes);
			$row->modes = $this->get_template_modes($userid, $row->templateid);
			$names[] = $row->productName;
			$templates[] = $row;
		}
		
		if (!in_array('Main meter', $names)) {
			$templates[] = $this->add_template($userid, 'Main meter', 'grid', 'input', 'The main energy metering unit in a household, measuring the total energy consumed from the public grid.', array( 'energyOut' ), array( 'default' ));
		}
		if (!in_array('Main meter (Bidirectional)', $names)) {
			$templates[] = $this->add_template($userid, 'Main meter (Bidirectional)', 'grid', 'input', 'The main energy metering unit in a household, measuring the total energy exchanged with the public grid.', array( 'energyOut', 'energyIn' ), array( 'default' ));
		}
		if (!in_array('Solar panel', $names)) {
			$templates[] = $this->add_template($userid, 'Solar panel', 'photovoltaic', 'input', 'A photovoltaic system generates electricity from solar energy. Solar panel devices represent installations, integrated by a single metering unit.', array( 'energyOut' ), array( 'default' ));
		}
		if (!in_array('Solar panel (Virtual)', $names)) {
			$templates[] = $this->add_template($userid, 'Solar panel (Virtual)', 'photovoltaic', 'input', 'A photovoltaic system generates electricity from solar energy. Solar panel devices represent installations, integrated by a single metering unit.', array( 'energyOut' ), array( 'default' ));
		}
		
		if (!in_array('Heat pump (Default)', $names)) {
			$templates[] = $this->add_template($userid, 'Heat pump (Default)', 'heat-pump', 'continuously_run', 'The default representation of a heat pump system, controlled by switching the systems power supply.', array( 'controller','energyIn' ), array( 'default' ));
		}
		if (!in_array('Air conditioner (Default)', $names)) {
			$templates[] = $this->add_template($userid, 'Air conditioner (Default)', 'air-conditioner', 'continuously_run', 'The default representation of a air conditioning system, controlled by switching the systems power supply.', array( 'controller','energyIn' ), array( 'default' ));
		}
		if (!in_array('Freezer (Default)', $names)) {
			$templates[] = $this->add_template($userid, 'Freezer (Default)', 'freezer', 'continuously_run', 'The default representation of a freezer, controlled by switching the systems power supply.', array( 'controller','energyIn' ), array( 'default' ));
		}
		if (!in_array('Refrigerator (Default)', $names)) {
			$templates[] = $this->add_template($userid, 'Refrigerator (Default)', 'refrigerator', 'continuously_run', 'The default representation of a refrigerator, controlled by switching the systems power supply.', array( 'controller','energyIn' ), array( 'default' ));
		}
		
		if (!in_array('Electric vehicle (Default)', $names)) {
			$templates[] = $this->add_template($userid, 'Electric vehicle (Default)', 'electric-vehicle', 'ev', 'The default representation of an electric vehicle.', array( 'controller','energyIn','soc' ), array( 'default' ));
		}
		
		if (!in_array('Dishwasher (Default)', $names)) {
			$templates[] = $this->add_template($userid, 'Dishwasher (Default)', 'dishwasher', 'single_run', 'The default representation of a dishwasher, controlled by switching the systems power supply.', array( 'controller','energyIn' ), array( 'default' ));
		}
		if (!in_array('Washing machine (Default)', $names)) {
			$templates[] = $this->add_template($userid, 'Washing machine (Default)', 'washing-machine', 'single_run', 'The default representation of a washing machine, controlled by switching the systems power supply.', array( 'controller','energyIn' ), array( 'default' ));
		}
		
		return $templates;
	}
	
	public function add_template($userid, $productName, $productType, $operatingType, $description, $requiredNodeTypes, $modes)
	{
		$requiredNodeTypes = implode(',', $requiredNodeTypes);
		$modes = array_unique($modes);
		
		$this->mysqli->query("INSERT INTO templates (productName, productType, operatingType, description, requiredNodeTypes, userid) VALUES ('".$this->mysqli->real_escape_string($productName)."', '".$this->mysqli->real_escape_string($productType)."', '".$this->mysqli->real_escape_string($operatingType)."', '".$this->mysqli->real_escape_string($description)."', '".$this->mysqli->real_escape_string($requiredNodeTypes)."', $userid)");
		$templateid= $this->mysqli->insert_id;
		
		foreach ($modes as $mode) {
			$this->mysqli->query("INSERT INTO templates_modes (templateid, modeName) VALUES ($templateid, '".$this->mysqli->real_escape_string($mode)."')");
		}
		$this->add_template_conf($templateid, $productType, $operatingType);
		
		return $this->get_template($userid, $templateid);
	}
	
	private function add_template_conf($templateid, $productType, $operatingType)
	{
		if ($operatingType == "single_run") {
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'default_EST','The earliest start time, a scheduled appliance will be allowed to run, when a device start was detected automatically.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'default_LST','The latest start time, a scheduled appliance will be allowed to run, when a device start was detected automatically.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'default_max_delay','The maximum amount of time in seconds, an appliance will be allowed to be delayed, when a device start was detected automatically.')");
		}
		if ($operatingType == "continuously_run" || $operatingType == "single_run") {
			// TODO: Aks Salvatore, what allowed_delay is
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'allowed_delay','0')");
			
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'noise','The noise power threshold in Watts, above which a device will be considered consuming power.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'silence','The amount of time waited, after a running device stopped consuming power, in seconds. A running device will be considered stopped only, after consuming no power above threshold for the specified amount of time.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'silence_start','The amount of time waited in seconds, after which a device consuming power will be considered to be running.')");
		}
		
		if ($productType == "pv") {
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'p_mpp','Maximum Power in watts.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'noct','Nominal operating cell temperature in degree Celsius.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'temp_coeff','MPP temperature coefficient in percent per Kelvin.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'n','Number of modules in the installation.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'tilt','Module tilt from horizontal in degree.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'azimuth','Module azimuth from north in degree.')");
			$this->mysqli->query("INSERT INTO template_conf VALUES ($templateid,'albedo','Module ground reflectance, typically 0.1-0.4 for surfaces on ground.')");
		}
		else if ($productType == "ev") {
			
		}
	}
	
	public function remove_template($userid, $templateid)
	{
		if (!is_numeric($templateid)) return array('success'=>false, 'message'=>'Invalid input parameter');
		
		$qresult = $this->mysqli->query("SELECT deviceid FROM devices WHERE templateid = $templateid");
		if ($qresult->num_rows > 0) return array('success'=>false, 'message'=>'Template is currently assigned to a device');
		
		$this->mysqli->query("DELETE FROM templates WHERE userid = $userid AND templateid = $templateid");
		if ($this->mysqli->affected_rows == 1) {
			$this->mysqli->query("DELETE FROM templates_modes WHERE templateid = $templateid");
			$this->mysqli->query("DELETE FROM template_conf WHERE templateid = $templateid");
				
			return array('success'=>true, 'message'=>'Template deleted');
		} else {
			return array('success'=>false, 'message'=>'Template does not exist or insufficient permissions');
		}
	}
	
	private function get_template_modes($userid, $templateid)
	{
		$qresult_modes = $this->mysqli->query("SELECT modeid,modeName FROM templates_modes WHERE templateid = ".$templateid);
		$modes = array();
		while($row_modes = $qresult_modes->fetch_object()) {
			$modes[] = $row_modes->modeid.":".$row_modes->modeName;
		}
		
		return $modes;
	}
	
	public function get_template_conf($userid, $templateid)
	{
		$qresult_parameters = $this->mysqli->query("SELECT name, description FROM template_conf WHERE idtemplate = ".$templateid);
		$conf = array();
		while($parameter = $qresult_parameters->fetch_object()) {
			$conf[] = $parameter;
		}
		
		return $conf;
	}

	public function get_parameter_info($userid, $templateid,$name)
	{
		$qresult_parameters = $this->mysqli->query("SELECT name, description FROM template_conf WHERE idtemplate = ".$templateid." and name='".$name."'");
		$conf = array();
		if($parameter = $qresult_parameters->fetch_object())
			$conf[] = $parameter;
		else 
			$conf[$name] = "missing";	
		return $conf;
	}
	
	public function register_node($userid, $driverid, $address, $type)
	{
		$node = $this->get_nodeid($userid, $driverid, $address);
		if ($node['success']) return array('success'=>false, 'message'=>'Node is already registered');
		
		$this->mysqli->query("INSERT INTO node_parameters (type, driverid, address, userid) VALUES ('".implode(',', $type)."', '".$this->mysqli->real_escape_string($driverid)."', '".$this->mysqli->real_escape_string($address)."', $userid)");
		$nodeid= $this->mysqli->insert_id;
		
		foreach ($type as $t) {
			if ($t == 'controller') $t = 'status';
			$this->input->create_input($userid, $nodeid, $t);
		}
		
		return array('success'=>true, 'nodeid'=>$nodeid, 'message'=>'Node successfully registered');
	}
	
	public function unregister_node($userid, $nodeid)
	{
		if (!is_numeric($nodeid)) return array('success'=>false, 'message'=>'Invalid input parameter');
		
		$qresult = $this->mysqli->query("SELECT deviceid FROM devices_nodes WHERE nodeid = $nodeid");
		if ($qresult->num_rows > 0) return array('success'=>false, 'message'=>'Node is currently assigned to a device');
		
		$this->mysqli->query("DELETE FROM node_parameters WHERE userid = $userid AND nodeid = $nodeid");
		
		if ($this->mysqli->affected_rows == 1) {
			return array('success'=>true, 'message'=>'Node unregistered');
		}
		else {
			return array('success'=>false, 'message'=>'Node does not exist');
		}
	}
	
	public function get_nodeid($userid, $driverid, $address)
	{
		
		$qresult = $this->mysqli->query("SELECT * FROM node_parameters WHERE userid = $userid AND driverid = '".$this->mysqli->real_escape_string($driverid)."' AND address = '".$this->mysqli->real_escape_string($address)."'");
		
		if ($qresult->num_rows == 0) return array('success'=>false, 'message'=>'No matching node');
		
		$node = $qresult->fetch_object();
		
		return array('success'=>true, 'nodeid'=>intval($node->nodeid));
		
	}
	
	public function get_node($userid, $nodeid)
	{
		if (!is_numeric($nodeid)) return array('success'=>false, 'message'=>'Invalid input parameter');
		
		$qresult = $this->mysqli->query("SELECT nodeid, type, driverid, address FROM node_parameters WHERE userid = $userid AND nodeid = $nodeid");
		
		if ($qresult->num_rows == 0) false;
		
		$node = $qresult->fetch_object();
		
		$node->nodeid = intval($node->nodeid);
		
		return $node;
		
	}
		
	public function get_unassigned_nodes($userid, $type = false)
	{
		if ($type) {
			$sql = "";
			foreach ($type as $t) $sql.= " AND FIND_IN_SET('".$this->mysqli->real_escape_string($t)."', p.type) > 0";
		}
		$qresult = $this->mysqli->query("SELECT p.nodeid, p.type, p.address FROM node_parameters p LEFT JOIN devices_nodes n ON n.userid = $userid AND n.nodeid = p.nodeid WHERE p.userid = $userid".($type ? $sql : "")." GROUP BY p.nodeid HAVING COUNT(n.nodeid) = 0");
		
		$nodes = array();
		while($row=$qresult->fetch_object()) {
			$row->type =explode(',', $row->type);
			$nodes[] = $row;
		}
		return $nodes;
	}
	
	private function set_household_processes($userid, $name, $type)
	{
		$qresult = $this->mysqli->query("SELECT n.nodeid, p.type FROM devices_nodes n LEFT JOIN node_parameters p ON p.nodeid = n.nodeid AND p.userid = $userid LEFT JOIN devices d ON n.deviceid = d.deviceid AND d.templateid = '".$name."' WHERE n.userid = $userid AND NOT ISNULL(d.deviceid)");
		
		$nodes = Array();
		while ($node = $qresult->fetch_object()) {
			if (in_array($type, explode(',', $node->type))) {
				$nodes[] = $node->nodeid;
			}
		}
		
		if (count($nodes) == 0) return false;
		
		if ($type == 'energyIn') $feed = 'in';
		elseif ($type == 'energyOut') $feed = 'out';
		else return false;
		
		$feedname = $name.'_'.$feed;
		if ($name == 'pv') $feedname = 'pv';
		
		$feed_kwh = $this->feed->get_id($userid, $name.'_'.$feed.'_kwh');
		$feed_power = $this->feed->get_id($userid, $name.'_'.$feed.'_power');
		$feed_kwhd = $this->feed->get_id($userid, $name.'_'.$feed.'_kwhd');
		
		$inputs = $this->input->get_inputs($userid);
		
		$processlist = '33:0,';
		foreach ($nodes as $node) {
			$processlist.= '11:'.$inputs[$node][$type]['id'].',';
		}
		$processlist.= '1:'.$feed_kwh.',21:'.$feed_power.',5:'.$feed_kwhd;
		
		foreach ($nodes as $node) {
			$this->input->set_processlist($inputs[$node][$type]['id'], $processlist);
		}
		
		return true;
	}
	
	public function add_device($userid, $name, $templateid, $nodes)
	{
		if (!is_numeric($templateid)) return array('success'=>false, 'message'=>'Invalid template ID parameter');
		
		$template = $this->get_template($userid, $templateid);
		$requiredNodeTypes = explode(',', $template->requiredNodeTypes);
		$operatingType = $template->operatingType;
		
		$qresult = $this->mysqli->query("SELECT nodeid, type FROM node_parameters WHERE $userid = $userid AND FIND_IN_SET(nodeid, '".$this->mysqli->real_escape_string(implode(',', $nodes))."') > 0");
		$selectedNodeTypes = Array();
		$selectedNodes = Array();
		while ($node = $qresult->fetch_object()) {
			$selectedNodeTypes = array_merge($selectedNodeTypes, explode(',', $node->type));
			$selectedNodes[$node->nodeid] = explode(',', $node->type);
			
			if ($this->get_device_by_nodeid($userid, $node->nodeid)) {
				return array('success'=>false, 'message'=>'Node '.$node->nodeid.' is already assigned to a device');
			}
		}
		
		$selectedNodeTypes = array_unique($selectedNodeTypes);
		
		if (!count($diff = array_diff($requiredNodeTypes, $selectedNodeTypes))) {
			$this->mysqli->query("INSERT INTO devices (name, templateid, userid) VALUES ('".$this->mysqli->real_escape_string($name)."', $templateid, $userid)");
			$deviceid = $this->mysqli->insert_id;
			
			while ($node = each($selectedNodes)) {
				$this->mysqli->query("INSERT INTO devices_nodes (userid, deviceid, nodeid) VALUES ($userid, $deviceid, ".$node['key'].")");
				
				$inputs = $this->input->get_inputs($userid);
				$inputs = $inputs[$node['key']];
				
				if (in_array('energyIn', $node['value'])) {
					$this->create_device_feeds($userid, $inputs['energyIn']['id'], 'device'.$deviceid.'_in', 'energy', $operatingType);
				}
				if (in_array('powerIn', $node['value'])) {
					$this->create_device_feeds($userid, $inputs['powerIn']['id'], 'device'.$deviceid.'_in', 'power', $operatingType);
					
					// TODO: Ask Salvatore if necessary
// 					$inputid = $this->input->create_input($userid, $node['key'], "energyIn");			
// 					$this->input->add_process($this->process, $userid, $inputid, 29, $feedid['energyIn_kwh']);
				}
				if (in_array('energyOut', $node['value'])) {
					$this->create_device_feeds($userid, $inputs['energyOut']['id'], 'device'.$deviceid.'_out', 'energy', $operatingType);
				}
				if (in_array('powerOut', $node['value'])) {
					$this->create_device_feeds($userid, $inputs['powerOut']['id'], 'device'.$deviceid.'_out', 'power', $operatingType);
					
					// TODO: Ask Salvatore if necessary
// 					$eOutid=$this->input->create_input($userid, $node['key'], "energyOut");			
// 					$this->input->add_process($this->process, $userid, $eOutid, 29, $feedid['energyOut_kwh']);
				}
				if (in_array('soc', $node['value'])) {
					$feedid = $this->feed->create($userid, $name,'device'.$deviceid.'_soc', DataType::REALTIME, Engine::PHPTIMESERIES, false);
					$this->input->add_process($this->process, $userid, $inputs['soc']['id'], 1, $feedid);
				}
				if (in_array('temperature', $node['value'])) {
					$feedid = $this->feed->create($userid, $name,'device'.$deviceid.'_temperature', DataType::REALTIME, Engine::PHPTIMESERIES, false);
					$this->input->add_process($this->process, $userid, $inputs['temperature']['id'], 1, $feedid);
				}
				if (in_array('flag', $node['value'])) {
					$feedid = $this->feed->create($userid, $name,'device'.$deviceid.'_flag', DataType::REALTIME, Engine::PHPTIMESERIES, false);
					$this->input->add_process($this->process, $userid, $inputs['flag']['id'], 1, $feedid);
				}
				if (in_array('controller', $node['value'])) {
					$feedid = $this->feed->create($userid, $name,'device'.$deviceid.'_status', DataType::REALTIME, Engine::PHPTIMESERIES, false);
					$this->input->add_process($this->process, $userid, $inputs['status']['id'], 1, $feedid);
				}
			}
			
			$qresult = $this->mysqli->query("select name from template_conf where idtemplate=$templateid");
			while ($row = $qresult->fetch_object()) {
				$this->mysqli->query("INSERT INTO user_device_par (deviceid,name,value) VALUES ($deviceid,'$row->name','')");
			}
			$this->add_operating_parameters($deviceid, $operatingType);
			
			return array('success'=>true, 'deviceid'=>$deviceid, 'message'=>'Device successfully added');
		}
		else {
			return array('success'=>false, 'message'=>'No nodes selected for '.implode(", ", $diff));
		}
	}

	private function create_device_feeds($userid, $inputid, $name, $inputType, $operatingType)
	{
		$this->log->info("Set process list for device ".$name."...\r\n");
		$this->input->reset_process($inputid);

		$kwhname = $name.'_kwh';
		$kwhid = $this->feed->get_id($userid, $kwhname);
		if (!$kwhid) {
			$this->log->info("Creating feed ".$kwhname."...\r\n");
				
			$result = $this->feed->create($userid, 'Household', $kwhname, DataType::REALTIME, Engine::PHPTIMESERIES, false);
			$kwhid = $result['feedid'];
		}
		
		$powername = $name.'_power';
		$powerid = $this->feed->get_id($userid, $powername);
		if (!$powerid) {
			$this->log->info("Creating feed ".$powername."...\r\n");
				
			$result = $this->feed->create($userid, 'Household', $powername, DataType::REALTIME, Engine::PHPTIMESERIES, false);
			$powerid = $result['feedid'];
		}
		
		$kwhdname = $name.'_kwhd';
		$kwhdid = $this->feed->get_id($userid, $kwhdname);
		if (!$kwhdid) {
			$this->log->info("Creating feed ".$kwhdname."...\r\n");
				
			$result = $this->feed->create($userid, 'Household', $kwhdname, DataType::DAILY, Engine::PHPTIMESERIES, false);
			$kwhdid = $result['feedid'];
		}
		
		if ($inputType === 'energy') {
			$this->input->add_process($this->process, $userid, $inputid, 1, $kwhid);
			$this->input->add_process($this->process, $userid, $inputid, 21, $powerid);

			if ($operatingType == "single_run" || $operatingType == "continuosly_run" || $operatingType=="continuous_running" || $operatingType == "ev") {
				$this->input->add_process($this->process, $userid, $inputid, 37, $inputid);
			}
			$this->input->add_process($this->process, $userid, $inputid, 5, $kwhdid);
		}
		else if ($inputType === 'power') {
			$this->input->add_process($this->process, $userid, $inputid, 1, $powerid);
			
			if ($operatingType == "single_run" || $operatingType == "continuosly_run" || $operatingType=="continuous_running" || $operatingType == "ev") {
				$this->input->add_process($this->process, $userid, $inputid, 37, $inputid);
			}
			$this->input->add_process($this->process, $userid, $inputid, 4, $kwhid);
			$this->input->add_process($this->process, $userid, $inputid, 23, $kwhdid);
		}
	}
	
	public function get_device_by_nodeid($userid, $nodeid)
	{
		if (!is_numeric($nodeid)) return array('success'=>false, 'message'=>'Invalid input parameter');
		
		$qresult = $this->mysqli->query("SELECT deviceid FROM devices_nodes WHERE userid = $userid AND nodeid = $nodeid");
		
		if ($qresult->num_rows == 0) return false;
		
		$device = $qresult->fetch_object();
		
		return array('success'=>true, 'deviceid'=>intval($device->deviceid));
	}
	
	public function get_device_by_deviceid($userid, $deviceid)
	{
		if (!is_numeric($deviceid)) return array('success'=>false, 'message'=>'Invalid input parameter');
		
		$qresult = $this->mysqli->query("SELECT deviceid, name, templateid FROM devices WHERE userid = $userid AND deviceid = $deviceid");
		
		if ($qresult->num_rows == 0) return false;
		
		$device = $qresult->fetch_assoc();
		
		$qresult = $this->mysqli->query("SELECT * FROM devices_nodes WHERE userid = $userid AND deviceid = $deviceid");
		
		$nodes = Array();
		while ($node = $qresult->fetch_object()) {
			$nodes[] = intval($node->nodeid);
		}
		
		$device['deviceid'] = intval($device['deviceid']);
		$device['templateid'] = intval($device['templateid']);
		$device['nodes'] = $nodes;
		
		return $device;
	}
	
	public function remove_device($userid, $deviceid)
	{
		if (!is_numeric($deviceid)) return array('success'=>false, 'message'=>'Invalid input parameter');
		
		$this->mysqli->query("DELETE FROM devices WHERE userid = $userid AND deviceid = $deviceid");
		if ($this->mysqli->affected_rows == 1) {
			
			$qresult = $this->mysqli->query("SELECT nodeid FROM devices_nodes WHERE userid = $userid AND deviceid = $deviceid");
			$inputs = $this->input->get_inputs($userid);
			
			while ($node = $qresult->fetch_object()) {
				foreach ($inputs[$node->nodeid] as $i) {
					$this->input->reset_process($i['id']);
				}
			}
			$this->mysqli->query("DELETE FROM devices_nodes WHERE userid = $userid AND deviceid = $deviceid");
			
			return array('success'=>true, 'message'=>'Device deleted');
		} else {
			return array('success'=>false, 'message'=>'Device does not exist or insufficient permissions');
		}
	}
		
	public function list_devices($userid)
	{
		$qresult = $this->mysqli->query("SELECT d.deviceid, d.name, p.nodeid AS controller FROM devices d LEFT JOIN devices_nodes n ON n.deviceid = d.deviceid LEFT JOIN node_parameters p ON p.nodeid = n.nodeid AND FIND_IN_SET('controller', p.type) > 0 WHERE d.userid = $userid GROUP BY deviceid ORDER BY name ASC");
		$qresult = $this->mysqli->query("SELECT d.deviceid, d.name, t.productType, t.operatingType, p.nodeid AS controller FROM devices d  JOIN templates t ON d.templateid=t.templateid JOIN devices_nodes n ON n.userid = $userid AND n.deviceid = d.deviceid  LEFT JOIN node_parameters p ON p.nodeid = n.nodeid AND FIND_IN_SET('controller', p.type) > 0 GROUP BY deviceid ORDER BY name ASC");
		$inputs = $this->input->get_inputs($userid);
		
		$devices = array();
		while($row=$qresult->fetch_object()) {
			
			if (is_numeric($row->controller) and in_array($row->controller, $inputs)) {
				$row->status = $this->input->get_last_value($inputs[$row->controller]['status']['id']);
			}
			else {
				$row->status = -1;
			}
			
			$row->id = $row->deviceid;
			$devices[] = $row; 
		}
		return $devices;
	}
	
	public function set_device_status($userid, $deviceid, $status)
	{
		global $cossmic;

		if(!$cossmic['control']) {
			
			$now = new DateTime();
			$this->log->warn($now->format('Y-m-d H:i').': Tried to set status for Device '.$deviceid.' to "'.$status.'" with disabled control');
			return array('success'=>false, 'message'=>'Device control disabled');
		}
		
		if (!$this->belongs_to_user($userid, $deviceid)) return array('success'=>false, 'message'=>'Insufficient permissions');
		
		$qresult = $this->mysqli->query("SELECT p.address, p.driverid FROM node_parameters p LEFT JOIN devices_nodes n ON n.userid = $userid AND p.nodeid = n.nodeid WHERE n.deviceid = $deviceid AND FIND_IN_SET('controller', p.type) > 0");
		echo $this->mysqli->error;
		
		while ($row = $qresult->fetch_object()) {
			$cmd = Array('address' => $row->address, 'command' => 'set', 'status' => $status);
			
			include "Modules/devices/$row->driverid/cmd.php";
		}
		
		return array('success'=>true);
	}
	
	public function post_value($userid, $driverid, $address, $type, $value, $time)
	{
		if (!is_numeric($value) or !is_numeric($time)) return array('success'=>false, 'message'=>'Invalid input parameter');
		
		$mtype = ($type == 'status') ? 'controller' : $type;
		
		$qresult = $this->mysqli->query("SELECT nodeid FROM node_parameters WHERE userid = $userid AND driverid = '".$this->mysqli->real_escape_string($driverid)."' AND address = '".$this->mysqli->real_escape_string($address)."' AND FIND_IN_SET('".$this->mysqli->real_escape_string($mtype)."', type) > 0");
		
		
		if ($qresult->num_rows == 0) {
			return array('success'=>false);
		}
		
		$node = $qresult->fetch_object();
	  
		$inputs = $this->input->get_inputs($userid);
		
		$this->input->set_timevalue($inputs[$node->nodeid][$type]['id'], $time, $value);
		if ($inputs[$node->nodeid][$type]['processList']) 
		 {	$this->process->input($time, $value, $inputs[$node->nodeid][$type]['processList']);
		}
		return array('success'=>true);
	}
	
	public function get_parameters($deviceid,$description)
	{
		$descriptions = array();
		if($description==True) {
			$qresult = $this->mysqli->query("select template_conf.name,template_conf.description from devices, template_conf where devices.templateid=template_conf.idtemplate and  deviceid=".$deviceid);
			$descriptions = array();
			while ($row = $qresult->fetch_object()) {
				$descriptions[$row->name]=$row->description;
			}
		}
		$deviceid = (int) $deviceid;
	  
		//if (!$this->redis->exists("user:devices:params:$deviceid")) 
		$this->load_params_to_redis($deviceid);
		
		$params = array();	  
		$paramsids = $this->redis->sMembers("user:devices:params:$deviceid");
		foreach ($paramsids as $id)
		{
			$row = $this->redis->hGetAll("devices:params:$id");
			if(array_key_exists($row['name'],$descriptions)) {
				$row['description']=$descriptions[$row['name']];
				
			}
            $params[] = $row;
		}
		return $params;
	}
	
	public function set_parameter($id,$fields)
	{
		$id = intval($id);
		
		$fields = json_decode(stripslashes($fields));
		
		//error_log("parameter:".$fields->value);
		if (isset($fields->value)) {
			$value=preg_replace('/[^\w\s-]/','',$fields->value);
			
			//error_log("parameter:".$value);
			$this->mysqli->query("UPDATE user_device_par SET value='".$fields->value."' WHERE `id` = '$id'");
			
			// if (isset($fields->value)) $this->redis->hset("driver:$id",'$id',$fields->value);
			/*
			// Repeat this line changing the field name to add fields that can be updated:
			if (isset($fields->description)) $array[] = "`description` = '".preg_replace('/[^\w\s-]/','',$fields->description)."'";
			if (isset($fields->name)) $array[] = "`name` = '".preg_replace('/[^\w\s-.]/','',$fields->name)."'";
			if (isset($fields->status)) $array[] = "`status` = '".$fields->status."'";
			// Convert to a comma seperated string for the mysql query
			
			$fieldstr = implode(",",$array);
			* 
			$this->mysqli->query("UPDATE user_drivers SET ".$fieldstr." WHERE `id` = '$id'");
			
			// CHECK REDIS?
			// UPDATE REDIS
			if (isset($fields->name)) $this->redis->hset("driver:$id",'name',$fields->name);
			if (isset($fields->description)) $this->redis->hset("driver:$id",'description',$fields->description);		
			if (isset($fields->status)) $this->redis->hset("driver:$id",'status',$fields->status);		
			*/
			
			if ($this->mysqli->affected_rows>0){
				$this->load_param_to_redis($id);
				return array('success'=>true, 'message'=>'Field updated');
			} else {
				return array('success'=>false, 'message'=>'Field could not be updated');
			}
		}
	}
	
	public function set_parameters($id, $fields)
	{
		$fields=strtr ($fields, array ("'" => '"'));
		$fields = json_decode(stripslashes($fields),true);
		
		foreach ($fields as $name => $value)
		{
			$this->mysqli->query("UPDATE user_device_par SET value='".$value."' WHERE `deviceid` = '$id' and name='".$name."'");
			$result = $this->mysqli->query("select id from user_device_par  WHERE `deviceid` = '$id' and name='".$name."'");
			if($row = $result->fetch_object()) {
				$this->load_param_to_redis($row->id);
			}
		}
		return "ok";
	}
	
	private function load_param_to_redis($paramid)
	{
		$result=$this->mysqli->query("SELECT * FROM user_device_par  WHERE `id` = '$paramid'");
		
		if($row = $result->fetch_object()) {
			$this->redis->hMSet("devices:params:$row->id",array(
				'id'=>$row->id,
				'name'=>$row->name,
				'value'=>$row->value
			));
		}
	}
	
	private function load_params_to_redis($deviceid)
	{
		$result = $this->mysqli->query("SELECT id,name,value FROM user_device_par WHERE `deviceid` = '$deviceid'");
		
		while($row = $result->fetch_object()){
			$this->redis->sAdd("user:devices:params:$deviceid", $row->id);
			$this->redis->hMSet("devices:params:$row->id",array(
				'id'=>$row->id,
				'name'=>$row->name,
				'value'=>$row->value
			));
		}
	}
}
