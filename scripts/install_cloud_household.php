<?php
	$cloudurl = 'http://cloud.cossmic.eu/emoncms/';
	$cloudapikey = 'YOUR_CLOUD_API_KEY';
	$household_id = 'X';


	class EMon {
		private $ch;
		private $url;
		private $apikey;
		
		function __construct($url, $apikey) {
			$this->url = $url;
			$this->apikey = $apikey;
			
			// initialize CURL
			$this->ch = curl_init();
 
			// set basic parameters
			curl_setopt($this->ch, CURLOPT_HEADER, 0);
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
		}
		
		function getInputs() {
			curl_setopt($this->ch, CURLOPT_URL, $this->url.'input/list.json?apikey='.$this->apikey);
			$data = curl_exec($this->ch);
			
			return (array) json_decode($data);
		}
		
		function getInputID($nodeID, $name) {
			$inputs = $this->getInputs();
			foreach ($inputs as $i) {
				$input = (array) $i;
				if ($input['nodeid'] == $nodeID && $input['name'] === $name) {
					return intval($input['id']);
				}
			}
			return -1;
		}
		
		function writeInput($nodeid, $name, $value, $timestamp) {
			curl_setopt($this->ch, CURLOPT_URL, $this->url.'input/post.json?apikey='.$this->apikey.'&node='.$nodeid.'&time='.$timestamp.'&json={'.$name.':'.$value.'}');
			$data = curl_exec($this->ch);
			
			return ($data == "ok");
		}
		
		function addFeed($name, $tag, $datatype, $engine) {
			curl_setopt($this->ch, CURLOPT_URL, $this->url.'feed/create.json?apikey='.$this->apikey.'&tag='.$tag.'&name='.$name.'&datatype='.$datatype.'&engine='.$engine);
			$data = (array) json_decode(curl_exec($this->ch));
			
			if (isset($data['success']) && $data['success']) return intval($data['feedid']);
			
			return false;
		}
		
		function addInputProcess($inputID, $processID, $arg) {
			curl_setopt($this->ch, CURLOPT_URL, $this->url.'input/process/add.json?apikey='.$this->apikey.'&inputid='.$inputID.'&processid='.$processID.'&arg='.$arg);
			$data = (array) json_decode(curl_exec($this->ch));
			
			if (isset($data['success']) && $data['success']) return true;
			
			return false;
		}
		
		function resetInputProcess($inputID) {
			curl_setopt($this->ch, CURLOPT_URL, $this->url.'input/process/reset.json?apikey='.$this->apikey.'&inputid='.$inputID);
			$data = (array) json_decode(curl_exec($this->ch));
		}
		
		function getFeedID($name) {
			curl_setopt($this->ch, CURLOPT_URL, $this->url.'feed/getid.json?apikey='.$this->apikey.'&name='.$name);
			$data = curl_exec($this->ch);
			
			return intval(str_replace("\"", "", $data));
		}
		
		function getFeedValue($id) {
			curl_setopt($this->ch, CURLOPT_URL, $this->url.'feed/value.json?apikey='.$this->apikey.'&id='.$id);
			$data = curl_exec($this->ch);
			
			return floatval(str_replace("\"", "", $data));
		}
		
		function getFeedTimeValue($id) {
			curl_setopt($this->ch, CURLOPT_URL, $this->url.'feed/timevalue.json?apikey='.$this->apikey.'&id='.$id);
			$data = (array) json_decode(curl_exec($this->ch));
			
			return array('time' => intval($data['time']), 'value' => floatval($data['value']));
		}
		
		function close() {
			curl_close($this->ch);
		}
	}
		
	function create_input_and_feeds($emon, $node, $input) {
		if ($node) {
			$prefix = $node.'_';
			$tag = 'Node:' + $node;
		}
		else {
			$prefix = '';
			$tag = 'Household';
		}
		
		$id = $emon->getInputID($node, $input);
		if ($id == -1) {
			echo "Creating input ".$input."...\r\n";
			$emon->writeInput($node, $input, 0, 0);
			$id = $emon->getInputID($node, $input);
		}

		$feed_kwh_name = $prefix.$input.'_kwh';
		$feed_kwh = $emon->getFeedID($feed_kwh_name);
		if (!$feed_kwh) {
			echo "Creating feed ".$feed_kwh_name."...\r\n";
			$feed_kwh = $emon->addFeed($feed_kwh_name, $tag, 1, 2);
		}

		$feed_power_name = $prefix.$input.'_power';
		$feed_power = $emon->getFeedID($feed_power_name);
		if (!$feed_power) {
			echo "Creating feed ".$feed_power_name."...\r\n";
			$feed_power = $emon->addFeed($feed_power_name, $tag, 1, 2);
		}

		$feed_kwhd_name = $prefix.$input.'_kwhd';
		$feed_kwhd = $emon->getFeedID($feed_kwhd_name);
		if (!$feed_kwhd) {
			echo "Creating feed ".$feed_kwhd_name."...\r\n";
			$feed_kwhd = $emon->addFeed($feed_kwhd_name, $tag, 2, 2);
		}
		
		return array('id' => $id, 'kwh' => $feed_kwh, 'power' => $feed_power, 'kwhd' => $feed_kwhd);
	}

	$cloud = new EMon($cloudurl, $cloudapikey);
 
	$inputs = Array('grid_out', 'grid_in', 'storage_out', 'storage_in', 'grid2household', 'grid2storage', 'storage2grid', 'storage2household', 
			'pv2grid', 'pv2household', 'pv2storage', 'pv', 'consumption');
		 
	echo "Installing cloud household feeds:\r\n";
	for ($i = 0; $i < count($inputs); $i++) {
		$input = $inputs[$i];
		
		$result = create_input_and_feeds($cloud, $household_id, $input);
		
		echo "Set process list for input ".$input."...\r\n";
		$cloud->resetInputProcess($result['id']);
		$cloud->addInputProcess($result['id'], 1, $result['kwh']);
		$cloud->addInputProcess($result['id'], 21, $result['power']);
		$cloud->addInputProcess($result['id'], 5, $result['kwhd']);
	}
?>