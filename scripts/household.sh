#!/bin/bash

# Set the locally installed emoncms API key
APIKEY='YOUR_API_KEY'

# The PV system does not feed-in directly into to grid, 
# but its energy will first be consumed in the household.
# Set the selfconsumption to false, if installed pv systems
# feed directly into the grid.
SELFCONSUMPTION=true


if [ $SELFCONSUMPTION = true ] ; then
	VALUE='1'
else
	VALUE='0'
fi
curl --silent --request GET 'http://localhost/emoncms/input/post.json?node=0&json={household:'$VALUE'}&apikey='$APIKEY >/dev/null
